#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -euo pipefail

: ${REPO:='https://gitlab.com/robinbb/Shpadoinkle-snowman'}

echo
echo "Let’s build a snowman!"
echo
echo "What should we call your snowman (e.g. tom, george, beowulf)?"

read -r name

if [[ -z "$name" ]]; then
  name="beowulf"
fi

echo 'Your snowman will be named "'"$name"'"'
echo "Where should $name live (e.g.  ./$name)?"
echo

read -r path

if [[ -z "$path" ]]; then
  path="./$name"
fi

mkdir -p "$path"

git clone --no-checkout "${REPO}" "$path"
cd "$path"

# Use the commit SHA being tested if we are in CI.
# Otherwise, use the head of the default branch ('master').
#
: ${CI_COMMIT_SHA:=HEAD}
git checkout "${CI_COMMIT_SHA}" -- project-template
git checkout "${CI_COMMIT_SHA}" -- success.gen

cp -r ./project-template/. .
rm -fr ./project-template

echo "Naming snowman $name …"
echo

# sed differs slightly between GNU, BSD, and POSIX
# https://riptutorial.com/sed/topic/9436/bsd-macos-sed-vs--gnu-sed-vs--the-posix-sed-specification
xOsReplace() {
  if ! sed --version | grep -q "(GNU sed)"; then
    xargs -0 sed -i '' -e "s/snowman/$name/g"
  else
    xargs -0 sed -i -e "s/snowman/$name/g"
  fi
}

mv ./haskell/snowman.cabal "./haskell/${name}.cabal"
rm -rf ".git"
find . -type f -print0 | xOsReplace

cat ./success.gen
rm ./success.gen
