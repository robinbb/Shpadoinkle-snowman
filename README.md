# Snowman

This repository is a template for
[Shpadoinkle](https://gitlab.com/fresheyeball/Shpadoinkle) projects.
It supports building with both Nix and Stack.

## Instructions

To create a new Shpadoinkle project from this template, invoke the following script:

```bash
bash <( curl -sSL https://gitlab.com/robinbb/Shpadoinkle-snowman/-/raw/master/generate )
```


### Using The Generated Project

The above script generates a new Shpadoinkle project with your choice of name.
To use the project, choose the method by which you intend to build (Nix or
Stack) and see the corresponding section, below.


#### Stack

Stack should work out of the box, provided you have the following installed on
your system:

- git
- zlib
- zlib-dev
- pcre

```bash
( cd haskell && stack build )
```


#### Nix

The generated `default.nix` file has some arguments to customize your build. To
build with GHC, invoke:

```bash
nix-build
```

To build with GHCjs, invoke:

```bash
nix-build --arg isJS true
```

To obtain a development environment, invoke:

```
nix-shell
```

This will drop you into a dev shell with
[Ghcid](https://github.com/ndmitchell/ghcid) and other common Haskell dev tools
available.

To invoke a ghcid server with live reloads in one line:

```bash
nix-shell --command "cd haskell && ghcid --command 'cabal repl' -W -T Main.dev"
```

To invoke a Hoogle server in one line:

```bash
nix-shell --arg withHoogle true --command "cd haskell && hoogle serve"
```
