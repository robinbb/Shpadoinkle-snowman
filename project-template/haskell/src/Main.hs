{-# LANGUAGE OverloadedStrings #-}


module Main where


import           Shpadoinkle                 (Html, JSM)
import           Shpadoinkle.Backend.ParDiff (runParDiff)
import           Shpadoinkle.Html
import           Shpadoinkle.Run             (live, runJSorWarp, simple)


view :: () -> Html m ()
view _ = "Hello, world!"


app :: JSM ()
app = simple runParDiff () view getBody


port :: Int
port = 8080

dev :: IO ()
dev = live port app


main :: IO ()
main = do
  putStrLn $ "\nHappy point of view on http://localhost:" <> show port
  runJSorWarp port app
