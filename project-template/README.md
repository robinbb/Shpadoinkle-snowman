# snowman

My best friend!

## Build snowman

```bash
nix-build
```

or

```bash
( cd haskell && stack build )
```

## Develop with live reloading

```bash
nix-shell --command "cd haskell && ghcid --command 'cabal repl' -W -T Main.dev"
```
