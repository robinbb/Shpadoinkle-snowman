#!/usr/bin/env nix-shell
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#! nix-shell -p bash -i bash -I nixpkgs="https://github.com/NixOS/nixpkgs/archive/nixos-20.09.tar.gz" --fallback

set -euo pipefail

build() {
  local MY_DIR="$(cd "$(dirname "$0")" && pwd)"
  "$MY_DIR"/cachix-build "$@"
}

# Build with defaults.
build --arg isJS false --no-out-link
build --arg isJS true --no-out-link

# Build with 'asShell'.
build --arg isJS false --arg asShell true --no-out-link
build --arg isJS false --arg asShell true --arg withHoogle true --no-out-link
build --arg isJS true  --arg asShell true --no-out-link

# TODO: test on macOS
#
# nix-build --argstr system x86_64-darwin --arg isJS false \
#   --no-out-link
# nix-build --argstr system x86_64-darwin --arg isJS true \
#   --no-out-link
# nix-build --argstr system x86_64-darwin --arg isJS false \
#   --arg asShell true --no-out-link
# nix-build --argstr system x86_64-darwin --arg isJS true \
#   --arg asShell true --no-out-link
# nix-build --argstr system x86_64-darwin --arg isJS false \
#   --arg asShell true --arg withHoogle true --no-out-link
