#! /usr/bin/env nix-shell
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#! nix-shell -p pcre git stack zlib glibcLocales haskell.compiler.ghc865 -i bash -I nixpkgs="https://github.com/NixOS/nixpkgs/archive/nixos-20.09.tar.gz" --fallback

set -eo pipefail

if [ "$1" ]; then
  TMP_DIR="$1"
else
  TMP_DIR="$(mktemp -d /tmp/snowman-XXXXXXXX)"
fi

set -u

PROJECT_NAME="$(basename "$TMP_DIR")"

on_exit() {
  # rm -fr "$TMP_DIR"
  :
  echo "test-stack done."
}

trap on_exit EXIT

: ${CI_COMMIT_SHA:="$(git rev-parse HEAD)"}
export CI_COMMIT_SHA

if [ ! -e "$TMP_DIR"/haskell ]; then
  cat <<EOF | bash ./generate
$PROJECT_NAME
$TMP_DIR
EOF
fi

FAKE_HOME="$TMP_DIR"/fake-home
mkdir -p "$FAKE_HOME"/.stack
export HOME="$FAKE_HOME"
export LANG=en_US.utf8
export LC_ALL=C.UTF-8
export NIX_PATH="nixpkgs=https://github.com/NixOS/nixpkgs/archive/nixos-20.09.tar.gz"
cat <<EOF > "$HOME"/.stack/config.yaml
  nix:
    packages:
    - zlib
    - zlib.dev
    - zlib.out
    - pcre
    enable: true
    add-gc-roots: true
  system-ghc: true
EOF

(cd "$TMP_DIR"/haskell && stack build)
